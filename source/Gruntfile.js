module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
    	pkg: grunt.file.readJSON('package.json'),

    	concat: {
    		js: {
    			src: [
            'resources/js/*.js', // All JS in the libs folder
            'resources/js/libs/*.js',
            'resources/js/vendor/*.js'
            ],
            dest: '../web/resources/js/production.js',
        },
            css: {
                src: [
            'resources/css/*.css'
            ],
            dest: '../web/resources/css/production.css',
        },

            
    },
    compass: {
        dist: {
          options: {
            sassDir: 'resources/sass',
            cssDir: 'resources/css',
            }
        }
    },
    uglify: {
    	js: {
    		src: '../web/resources/js/production.js',
    		dest: '../web/resources/js/production.min.js'
    	}
    },
    
    watch: {
    	options: {
    		livereload: true,
    	},
    	scripts: {
    		files: [
    		'resources/js/*.js', 
    		'resources/js/libs/*.js',
    		'resources/js/vendor/*.js'
    		],
    		tasks: ['concat', 'uglify'],
    		options: {
    			spawn: false,
    		},
    	} ,
    	css: {
    		files: ['resources/sass/*.scss','resources/sass/bootstrap/*.scss'],
    		tasks: ['compass','concat'],
    		options: {
    			spawn: false,
    		}
    	},
        
          html: {
            files: ['../web/*.html','../web/resources/css/*.css'],
            options: {
                livereload: true
            }
        }
    }

});

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat', 'uglify', 'compass', 'watch']);

};