<?php
/**
 * @package contactsubmissions
 * @subpackage processors
 */
class ContactSubmissionUpdateProcessor extends modObjectUpdateProcessor {
    public $classKey = 'ContactSubmission';
    public $languageTopics = array('contactsubmissions:default');
    public $objectType = 'contactsubmissions.contactsubmission';
}
return 'ContactSubmissionUpdateProcessor';